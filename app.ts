interface Flyable {
    fly():void;
}
class Animal {
    Color: string;
}
class Bird extends Animal implements Flyable {
    Name: string;
    constructor(Name: string, Color: string) {
        super();
        this.Color = Color;
        this.Name = Name;
    }
    fly = ():void => console.log(`${this.Name} color ${this.Color} is flying`);
}
let bird1 = new Bird("Egle", "white");
let bird2 = new Bird("กระจิบ", "brown");

bird1.fly();
bird2.fly();

