var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Animal = /** @class */ (function () {
    function Animal() {
    }
    return Animal;
}());
var Bird = /** @class */ (function (_super) {
    __extends(Bird, _super);
    function Bird(Name, Color) {
        var _this = _super.call(this) || this;
        _this.fly = function () { return console.log("".concat(_this.Name, " color ").concat(_this.Color, " is flying")); };
        _this.Color = Color;
        _this.Name = Name;
        return _this;
    }
    return Bird;
}(Animal));
var bird1 = new Bird("Egle", "white");
var bird2 = new Bird("กระจิบ", "brown");
bird1.fly();
bird2.fly();
//# sourceMappingURL=app.js.map